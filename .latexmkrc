# vim: filetype=perl
$pdf_mode = 1;  # 2 --> create pdf using latex + dvips + ps2pdf
$pdflatex = 'pdflatex -synctex=1 -halt-on-error -file-line-error %O %S';
$clean_ext = '%R.bbl %R-blx.bib %R.run.xml %R.tdo';
$clean_ext = $clean_ext . ' %RNotes.bib';  # REVTeX stuff
$clean_full_ext = '%R.synctex.gz';

# Automatically convert svg files to pdf.
add_cus_dep('svg', 'pdf', 0, 'svg2pdf');
sub svg2pdf {
    system("inkscape --export-area-drawing" .
           " --export-pdf=\"$_[0].pdf\" \"$_[0].svg\"");
}

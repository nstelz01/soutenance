%% Creator: Inkscape inkscape 0.48.5, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'Autocorrelation_Vy.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{1536bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.49114583)%
    \put(0,0){\includegraphics[width=\unitlength]{Autocorrelation_Vy.pdf}}%
    \put(0.13020833,0.03694443){\makebox(0,0)[b]{\smash{0}}}%
    \put(0.2409226,0.03694443){\makebox(0,0)[b]{\smash{10}}}%
    \put(0.35163693,0.03694443){\makebox(0,0)[b]{\smash{20}}}%
    \put(0.4623512,0.03694443){\makebox(0,0)[b]{\smash{30}}}%
    \put(0.57306552,0.03694443){\makebox(0,0)[b]{\smash{40}}}%
    \put(0.68377979,0.03694443){\makebox(0,0)[b]{\smash{50}}}%
    \put(0.79449406,0.03694443){\makebox(0,0)[b]{\smash{60}}}%
    \put(0.90520833,0.03694443){\makebox(0,0)[b]{\smash{70}}}%
    \put(0.5177087,0.01){\makebox(0,0)[b]{\smash{$t^+$}}}%
    \put(0.12465276,0.0829487){\makebox(0,0)[rb]{\smash{0}}}%
    \put(0.12465276,0.14384615){\makebox(0,0)[rb]{\smash{0.2}}}%
    \put(0.12465276,0.20474359){\makebox(0,0)[rb]{\smash{0.4}}}%
    \put(0.12465276,0.26564104){\makebox(0,0)[rb]{\smash{0.6}}}%
    \put(0.12465276,0.32653844){\makebox(0,0)[rb]{\smash{0.8}}}%
    \put(0.12465276,0.38743589){\makebox(0,0)[rb]{\smash{1}}}%
    \put(0.12465276,0.44833333){\makebox(0,0)[rb]{\smash{1.2}}}%
    \put(0.06381943,0.25625021){\rotatebox{90}{\makebox(0,0)[b]{\smash{Normalized autocorrelation}}}}%
    \put(0.51770885,0.46304688){\makebox(0,0)[b]{\smash{Autocorrelation of wall-normal velocity}}}%
    \put(0.70520833,0.42959063){\makebox(0,0)[lb]{\smash{$y^+=0-38$}}}%
    \put(0.70520833,0.40682937){\makebox(0,0)[lb]{\smash{$y^+=38-75$}}}%
    \put(0.70520833,0.38406818){\makebox(0,0)[lb]{\smash{$y^+=75-113$}}}%
    \put(0.70520833,0.36130698){\makebox(0,0)[lb]{\smash{$y^+=113-151$}}}%
    \put(0.70520833,0.33854573){\makebox(0,0)[lb]{\smash{$y^+=151-189$}}}%
    \put(0.70520833,0.31578448){\makebox(0,0)[lb]{\smash{$y^+=189-226$}}}%
    \put(0.70520833,0.29302323){\makebox(0,0)[lb]{\smash{$y^+=226-302$}}}%
    \put(0.70520833,0.27026203){\makebox(0,0)[lb]{\smash{$y^+=302-377$}}}%
    \put(0.70520833,0.24750078){\makebox(0,0)[lb]{\smash{$y^+=377-453$}}}%
    \put(0.70520833,0.22473958){\makebox(0,0)[lb]{\smash{$y^+=453-528$}}}%
    \put(0.70520833,0.20197839){\makebox(0,0)[lb]{\smash{$y^+=528-604$}}}%
    \put(0.70520833,0.17921714){\makebox(0,0)[lb]{\smash{$y^+=604-679$}}}%
    \put(0.70520833,0.15645589){\makebox(0,0)[lb]{\smash{$y^+=679-755$}}}%
    \put(0.70520833,0.13369469){\makebox(0,0)[lb]{\smash{$y^+=755-830$}}}%
    \put(0.70520833,0.11093344){\makebox(0,0)[lb]{\smash{$y^+=830-906$}}}%
    \put(0.70520833,0.08817219){\makebox(0,0)[lb]{\smash{$y^+=906-981$}}}%
    \put(0.70520833,0.06541099){\makebox(0,0)[lb]{\smash{$y^+=981-1057$}}}%
    \put(0.70520833,0.04264974){\makebox(0,0)[lb]{\smash{$y^+=1057-1132$}}}%
    \put(0.70520833,0.01988854){\makebox(0,0)[lb]{\smash{$y^+=1132-1208$}}}%
  \end{picture}%
\endgroup%

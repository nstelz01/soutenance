\beamer@endinputifotherversion {3.33pt}
\select@language {english}
\beamer@sectionintoc {2}{Experimental methods}{34}{0}{1}
\beamer@sectionintoc {3}{Lagrangian statistics of inhomogeneous turbulence }{62}{0}{2}
\beamer@sectionintoc {4}{Non-tracer particle Lagrangian statistics}{79}{0}{3}

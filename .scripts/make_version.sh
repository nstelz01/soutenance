#!/bin/bash

TMPDIR=$(mktemp -d)

# Delete $TMPDIR at the end, even if there's an error.
# Taken from http://redsymbol.net/articles/bash-exit-traps
function finish() {
    echo "Deleting $TMPDIR"
    rm -rf "$TMPDIR"
}
trap finish EXIT

if [[ ! -d .git ]]; then
    echo "The script must be called from the root of the git repo."
    exit 1
fi

git clone . "$TMPDIR"
pushd "$TMPDIR"

# Determine main LaTeX file.
TEXFILE=$(ls -1 -- *.tex)        # e.g. "article.tex"
NUMFILES=$(echo "$TEXFILE" | wc -l)  # number of files found by ls
if [[ "$NUMFILES" -gt 1 ]]; then
    echo "Error: more than one .tex file in the root directory."
    exit 2
fi

BASENAME=${TEXFILE/.tex/}       # e.g. "article"
VERSION=$(git describe --tags)
make pdf
popd

mkdir -p versions
cp -av "$TMPDIR/$BASENAME.pdf" versions/"$BASENAME"-"$VERSION".pdf

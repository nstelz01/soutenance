%% Creator: Inkscape inkscape 0.48.5, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'Vel_timescales.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{1536bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.49114583)%
    \put(0,0){\includegraphics[width=\unitlength]{./figures/Timescales/Vel_timescales2.pdf}}%
    \put(0.13020833,0.03694443){\makebox(0,0)[b]{\smash{0}}}%
    \put(0.38854167,0.03694443){\makebox(0,0)[b]{\smash{500}}}%
    \put(0.646875,0.03694443){\makebox(0,0)[b]{\smash{1000}}}%
    \put(0.90520833,0.03694443){\makebox(0,0)[b]{\smash{1500}}}%
    \put(0.5177087,0.01){\makebox(0,0)[b]{\smash{$y^+$}}}%
    \put(0.12465276,0.0525){\makebox(0,0)[rb]{\smash{0}}}%
    \put(0.12465276,0.11847224){\makebox(0,0)[rb]{\smash{100}}}%
    \put(0.12465276,0.18444443){\makebox(0,0)[rb]{\smash{200}}}%
    \put(0.12465276,0.25041667){\makebox(0,0)[rb]{\smash{300}}}%
    \put(0.12465276,0.31638891){\makebox(0,0)[rb]{\smash{400}}}%
    \put(0.12465276,0.38236109){\makebox(0,0)[rb]{\smash{500}}}%
    \put(0.12465276,0.44833333){\makebox(0,0)[rb]{\smash{600}}}%
    \put(0.06381943,0.25625016){\rotatebox{90}{\makebox(0,0)[b]{\smash{$T_L$}}}}%
    \put(0.51770885,0.46304688){\makebox(0,0)[b]{\smash{Lagrangian velocity timescale}}}%
    \put(0.1640625,0.42960068){\makebox(0,0)[lb]{\smash{$T_{L}$ (streamwise, exp.)}}}%
    \put(0.1640625,0.40685766){\makebox(0,0)[lb]{\smash{$T_{L}$ (wall-normal, exp.)}}}%
    \put(0.1640625,0.38411458){\makebox(0,0)[lb]{\smash{$T_{L}$ (spanwise, exp.)}}}%
    \put(0.1640625,0.36137151){\makebox(0,0)[lb]{\smash{$T_{L}$ (streamwise, DNS)}}}%
    \put(0.1640625,0.33862849){\makebox(0,0)[lb]{\smash{$T_{L}$ (wall-normal, DNS)}}}%
    \put(0.1640625,0.31588542){\makebox(0,0)[lb]{\smash{$T_{L}$ (spanwise, DNS)}}}%
    \put(0.1640625,0.29314234){\makebox(0,0)[lb]{\smash{Sawford scaling for HIT calculated with DNS}}}%
    %\put(0.1640625,0.27039932){\makebox(0,0)[lb]{\smash{Sawford scaling for HIT calculated with Kolmogorov time}}}%
  \end{picture}%
\endgroup%

%% Creator: Inkscape inkscape 0.48.5, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'a_ya_x.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{1536bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.49114583)%
    \put(0,0){\includegraphics[width=\unitlength]{a_ya_x.pdf}}%
    \put(0.1,0.02756943){\makebox(0,0)[b]{\smash{0}}}%
    \put(0.185,0.02756943){\makebox(0,0)[b]{\smash{10}}}%
    \put(0.27,0.02756943){\makebox(0,0)[b]{\smash{20}}}%
    \put(0.355,0.02756943){\makebox(0,0)[b]{\smash{30}}}%
    \put(0.44,0.02756943){\makebox(0,0)[b]{\smash{40}}}%
    \put(0.525,0.02756943){\makebox(0,0)[b]{\smash{50}}}%
    \put(0.61,0.02756943){\makebox(0,0)[b]{\smash{60}}}%
    \put(0.695,0.02756943){\makebox(0,0)[b]{\smash{70}}}%
    \put(0.78,0.02756943){\makebox(0,0)[b]{\smash{80}}}%
    \put(0.865,0.02756943){\makebox(0,0)[b]{\smash{90}}}%
    \put(0.95,0.02756943){\makebox(0,0)[b]{\smash{100}}}%
    \put(0.52500036,0.000625){\makebox(0,0)[b]{\smash{$\tau^+$}}}%
    \put(0.09444443,0.043125){\makebox(0,0)[rb]{\smash{-600}}}%
    \put(0.09444443,0.08921875){\makebox(0,0)[rb]{\smash{-400}}}%
    \put(0.09444443,0.1353125){\makebox(0,0)[rb]{\smash{-200}}}%
    \put(0.09444443,0.18140625){\makebox(0,0)[rb]{\smash{0}}}%
    \put(0.09444443,0.2275){\makebox(0,0)[rb]{\smash{200}}}%
    \put(0.03361109,0.14114594){\rotatebox{90}{\makebox(0,0)[b]{\smash{$a_y(t)a_x(t+\tau)$}}}}%
    \put(0.09444443,0.25197917){\makebox(0,0)[rb]{\smash{-0.4}}}%
    \put(0.09444443,0.29807292){\makebox(0,0)[rb]{\smash{-0.2}}}%
    \put(0.09444443,0.34416667){\makebox(0,0)[rb]{\smash{0}}}%
    \put(0.09444443,0.39026042){\makebox(0,0)[rb]{\smash{0.2}}}%
    \put(0.09444443,0.43635417){\makebox(0,0)[rb]{\smash{0.4}}}%
    \put(0.03361109,0.3500001){\rotatebox{90}{\makebox(0,0)[b]{\smash{$a_y(t)a_x(t+\tau)$ Normalized}}}}%
    \put(0.52500047,0.45106771){\makebox(0,0)[b]{\smash{Lagrangian correlations $a_y(t)a_x(t+\tau)$ (Eulerian means subtracted) at various wall-distances ($\Delta y$ = 1mm)}}}%
    \put(0.7265625,0.41760958){\makebox(0,0)[lb]{\smash{$y^+ = 0 - 75$}}}%
    \put(0.7265625,0.394845){\makebox(0,0)[lb]{\smash{$y^+ = 75 - 151$}}}%
    \put(0.7265625,0.37208036){\makebox(0,0)[lb]{\smash{$y^+ = 151 - 226$}}}%
    \put(0.7265625,0.34931578){\makebox(0,0)[lb]{\smash{$y^+ = 226 - 302$}}}%
    \put(0.7265625,0.32655115){\makebox(0,0)[lb]{\smash{$y^+ = 302 - 377$}}}%
    \put(0.7265625,0.30378656){\makebox(0,0)[lb]{\smash{$y^+ = 377 - 453$}}}%
    \put(0.7265625,0.28102193){\makebox(0,0)[lb]{\smash{$y^+ = 453 - 528$}}}%
    \put(0.7265625,0.25825729){\makebox(0,0)[lb]{\smash{$y^+ = 528 - 604$}}}%
    \put(0.7265625,0.23549271){\makebox(0,0)[lb]{\smash{$y^+ = 604 - 679$}}}%
    \put(0.7265625,0.21272807){\makebox(0,0)[lb]{\smash{$y^+ = 679 - 755$}}}%
    \put(0.7265625,0.18996349){\makebox(0,0)[lb]{\smash{$y^+ = 755 - 830$}}}%
    \put(0.7265625,0.16719885){\makebox(0,0)[lb]{\smash{$y^+ = 830 - 906$}}}%
    \put(0.7265625,0.14443422){\makebox(0,0)[lb]{\smash{$y^+ = 906 - 981$}}}%
    \put(0.7265625,0.12166964){\makebox(0,0)[lb]{\smash{$y^+ = 981 - 1057$}}}%
    \put(0.7265625,0.098905){\makebox(0,0)[lb]{\smash{$y^+ = 1057 - 1132$}}}%
    \put(0.7265625,0.07614042){\makebox(0,0)[lb]{\smash{$y^+ = 1132 - 1208$}}}%
  \end{picture}%
\endgroup%

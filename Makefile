.PHONY: pdf clean

all: pdf

pdf:
	latexmk

clean:
	latexmk -C

.PHONY: version
version:
	./.scripts/make_version.sh
